// ignore_for_file: file_names, depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/Widgets/app_images.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StatisticsPage extends StatelessWidget {
  final List<DataPoint> dataPoints = [
    DataPoint('Jan', 100),
    DataPoint('Feb', 200),
    DataPoint('Mar', 150),
    DataPoint('Apr', 300),
    DataPoint('May', 250),
  ];

   StatisticsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // appBar: AppBar(
        //   title: Text('Statistics'),
        // ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppPics.getRandomColor()),
                fit: BoxFit.cover,
                opacity: 0.2),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                const Text(
                  'Sales Statistics',
                  style: TextStyle(
                    fontFamily: 'OpenSans',
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16.0),
                Expanded(
                  child: ListView(
                    children: [
                      _buildChart(
                          'Column Chart',
                          SfCartesianChart(
                            // backgroundColor: AppColors.thirdColor,
                            primaryXAxis: CategoryAxis(),
                            series: <ChartSeries>[
                              ColumnSeries<DataPoint, String>(
                                color: AppColors.getRandomColor(),
                                dataSource: dataPoints,
                                xValueMapper: (DataPoint data, _) => data.month,
                                yValueMapper: (DataPoint data, _) =>
                                    data.amount,
                              ),
                            ],
                          )),
                      _buildChart(
                          'Line Chart',
                          SfCartesianChart(
                            primaryXAxis: CategoryAxis(),
                            series: <ChartSeries>[
                              LineSeries<DataPoint, String>(
                                color: AppColors.getRandomColor(),
                                dataSource: dataPoints,
                                xValueMapper: (DataPoint data, _) => data.month,
                                yValueMapper: (DataPoint data, _) =>
                                    data.amount,
                              ),
                            ],
                          )),
                      _buildChart(
                          'Pie Chart',
                          SfCircularChart(
                            series: <CircularSeries>[
                              PieSeries<DataPoint, String>(
                                //color: AppColors.getRandomColor(),
                                dataSource: dataPoints,
                                xValueMapper: (DataPoint data, _) => data.month,
                                yValueMapper: (DataPoint data, _) =>
                                    data.amount,
                              ),
                            ],
                          )),
                      _buildChart(
                          'Bar Chart',
                          SfCartesianChart(
                            primaryXAxis: CategoryAxis(),
                            series: <ChartSeries>[
                              BarSeries<DataPoint, String>(
                                color: AppColors.getRandomColor(),
                                dataSource: dataPoints,
                                xValueMapper: (DataPoint data, _) => data.month,
                                yValueMapper: (DataPoint data, _) =>
                                    data.amount,
                              ),
                            ],
                          )),
                      _buildChart(
                          'Area Chart',
                          SfCartesianChart(
                            primaryXAxis: CategoryAxis(),
                            series: <ChartSeries>[
                              AreaSeries<DataPoint, String>(
                                color: AppColors.getRandomColor(),
                                dataSource: dataPoints,
                                xValueMapper: (DataPoint data, _) => data.month,
                                yValueMapper: (DataPoint data, _) =>
                                    data.amount,
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildChart(String title, Widget chart) {
    return Card(
      elevation: 4.0,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: const TextStyle(
                fontFamily: 'OpenSans',
                fontSize: 20.0,
                //fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 16.0),
            SizedBox(
              height: 300.0,
              child: chart,
            ),
          ],
        ),
      ),
    );
  }
}

class DataPoint {
  final String month;
  final double amount;

  DataPoint(this.month, this.amount);
}
