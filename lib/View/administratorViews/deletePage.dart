// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/Controller/admin_controller.dart';

class DeletePage extends GetView<AdminController> {
  @override
  final controller = AdminController();

  DeletePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Delete'),
      ),
      body: Obx(
        () => ListView.builder(
          itemCount: controller.salespersons.length,
          itemBuilder: (context, index) {
            final salesperson = controller.salespersons[index];
            return Card(
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: salesperson.photo != null
                      ? AssetImage(salesperson.photo.toString())
                      : const AssetImage("assets/images/unknownPerson.jpg"),
                ),
                title: Text(salesperson.name.toString()),
                subtitle: Text(salesperson.region.toString()),
                trailing: IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () {
                    // Delete salesperson
                    Get.defaultDialog(
                      title: 'Delete Salesperson',
                      content: Text(
                          'Are you sure you want to delete ${salesperson.name}?'),
                      confirm: ElevatedButton(
                        onPressed: () {
                          controller.salespersons.removeAt(index);
                          controller.update();
                          Get.back();
                        },
                        child: const Text('Delete'),
                      ),
                      cancel: ElevatedButton(
                        onPressed: () {
                          Get.back();
                        },
                        child: const Text('Cancel'),
                      ),
                    );
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
