// ignore_for_file: avoid_print, file_names, depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:salesperson/Controller/admin_controller.dart';
import 'package:salesperson/Model/sales_person_model.dart';

class AddPage extends GetView<AdminController> {
  @override
  final controller = AdminController();
  final _formKey = GlobalKey<FormState>();
  final _salesperson = Salesperson(id: 0, name: '', photo: '', region: '');
  AddPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: [
              TextFormField(
                decoration: const InputDecoration(labelText: 'Number'),
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a number';
                  }
                  return null;
                },
                onSaved: (value) {
                  _salesperson.id = int.parse(value!);
                },
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Name'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a name';
                  }
                  return null;
                },
                onSaved: (value) {
                  _salesperson.name = value!;
                },
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Photo'),
                validator: (value) {
                  if (controller.image == null) {
                    return 'Please pick an image';
                  }
                  return null;
                },
                onTap: pickImage, // Added onTap callback
                readOnly: true, // Added readOnly property
                controller: TextEditingController(
                  text: controller.image?.path ??
                      '', // Added controller with initial value
                ),
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Region'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a region';
                  }
                  return null;
                },
                onSaved: (value) {
                  _salesperson.region = value!;
                },
              ),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();
                    controller.addSalesperson(_salesperson);
                    Get.back();
                  }
                },
                child: const Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> pickImage() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: ImageSource.gallery);
    controller.image = pickedImage;
  }
}
