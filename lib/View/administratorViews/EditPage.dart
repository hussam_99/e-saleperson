// ignore_for_file: file_names, depend_on_referenced_packages, avoid_print

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salesperson/View/Widgets/app_images.dart';

class EditPage extends StatelessWidget {
  const EditPage({super.key});

  @override
  Widget build(BuildContext context) {
    // Salesperson salesperson = SalespersonController.selectedSalesperson.value;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Salesperson'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextFormField(
                initialValue: "Hi", //salesperson.name,
                onChanged: (value) {
                  // SalespersonController.updateSalespersonName();
                },
                decoration: const InputDecoration(
                  labelText: 'Name',
                ),
              ),
              const SizedBox(height: 16.0),
              TextFormField(
                // initialValue: salesperson.region,
                onChanged: (value) {
                  print("Hello");
                  //SalespersonController.updateSalespersonRegion(value);
                },
                decoration: const InputDecoration(
                  labelText: 'Region',
                ),
              ),
              const SizedBox(height: 32.0),
              ElevatedButton(
                onPressed: () {
                  // SalespersonController.updateSalesperson();
                  Get.back();
                },
                child: const Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
