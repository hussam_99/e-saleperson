// ignore_for_file: depend_on_referenced_packages

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:salesperson/constant/app_route.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 500), () {
      Get.offNamed(AppRoute.login); // Navigate to login page after 5 seconds
    });
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(width: 300.w, height: 300.w, child: const AnimatedLogo()),
            const AnimatedText(),
            const CircularProgressIndicator()
          ],
        ),
      ),
    );
  }
}

class AnimatedLogo extends StatelessWidget {
  const AnimatedLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      init: SplashController(),
      builder: (controller) {
        return Opacity(
            opacity: controller.logoOpacity,
            child: Image.asset(
                "assets/images/logo/logo.png") //FlutterLogo(size: 150),
            );
      },
    );
  }
}

class AnimatedText extends StatelessWidget {
  const AnimatedText({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      init: SplashController(),
      builder: (controller) {
        return Opacity(
          opacity: controller.textOpacity,
          child: const Text(
            'E-Sale Person',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
        );
      },
    );
  }
}

class SplashController extends GetxController {
  double logoOpacity = 0.0;
  double textOpacity = 0.0;

  @override
  void onInit() {
    super.onInit();
    animateItems();
  }

  void animateItems() async {
    await Future.delayed(const Duration(milliseconds: 500));
    logoOpacity = 1.0;
    update();

    await Future.delayed(const Duration(milliseconds: 500));
    textOpacity = 1.0;
    update();

    await Future.delayed(const Duration(seconds: 2));
    Get.offNamed(AppRoute.login); // Replace '/login' with your login page route
  }
}
