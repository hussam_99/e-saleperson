// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:salesperson/View/Widgets/app_colors.dart';
import 'package:salesperson/View/Widgets/app_images.dart';
import 'package:salesperson/View/pages/homePage.dart';

class ScorePage extends StatelessWidget {
  final double score;
  final String salespersonNumber;
  final String salespersonName;
  final String month;
  final String year;
  final String registrationDate;
  final String southernRegion;
  final String coastalRegion;
  final String northernRegion;
  final String easternRegion;
  final String lebanon;
  final String monthlyCommission;

  const ScorePage({
    super.key,
    required this.score,
    required this.salespersonNumber,
    required this.salespersonName,
    required this.month,
    required this.year,
    required this.registrationDate,
    required this.southernRegion,
    required this.coastalRegion,
    required this.northernRegion,
    required this.easternRegion,
    required this.lebanon,
    required this.monthlyCommission,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Score Page'),
      // ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppPics.getRandomColor()),
              fit: BoxFit.cover,
              opacity: 0.2),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  width: Get.width,
                  height: Get.height - 100.h,
                  child: StaggeredGridView.countBuilder(
                    crossAxisCount: 2,
                    itemCount: 12,
                    itemBuilder: (BuildContext context, int index) {
                      return buildGridItem(index);
                    },
                    staggeredTileBuilder: (int index) {
                      return const StaggeredTile.fit(1);
                    },
                    mainAxisSpacing: 16.0,
                    crossAxisSpacing: 16.0,
                  ),
                ),
                OutlinedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            AppColors.thirdColor)),
                    onPressed: () {
                      Get.off(const HomePage());
                    },
                    child: Text(
                      "Back",
                      style: TextStyle(
                          fontFamily: 'OpenSans', color: AppColors.mainColor),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildGridItem(int index) {
    switch (index) {
      case 0:
        return buildGridItemWidget('Salesperson Number', salespersonNumber);
      case 1:
        return buildGridItemWidget('Salesperson Name', salespersonName);
      case 2:
        return buildGridItemWidget('Month', month);
      case 3:
        return buildGridItemWidget('Year', year);
      case 4:
        return buildGridItemWidget('Registration Date', registrationDate);
      case 5:
        return buildGridItemWidget('Southern Region', southernRegion);
      case 6:
        return buildGridItemWidget('Coastal Region', coastalRegion);
      case 7:
        return buildGridItemWidget('Northern Region', northernRegion);
      case 8:
        return buildGridItemWidget('Eastern Region', easternRegion);
      case 9:
        return buildGridItemWidget('Lebanon', lebanon);
      case 10:
        return buildGridItemWidget('Monthly Commission', monthlyCommission);
      case 11:
        return buildGridItemWidget('Score', score.toString());
      default:
        return Container();
    }
  }

  Widget buildGridItemWidget(String label, String value) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.getRandomColor(),
        borderRadius: BorderRadius.circular(8.0),
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: TextStyle(
              color: AppColors.whiteColor,
              //fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
          const SizedBox(height: 8.0),
          Text(
            value,
            style: TextStyle(
              color: AppColors.whiteColor,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ],
      ),
    );
  }
}
