import 'dart:math';

import 'package:flutter/material.dart';

class AppColors {
  static Color mainColor = const Color(0xFF494F58);
  static Color secondColor = const Color(0xFF54bee5);
  static Color thirdColor = const Color(0xFFfeae48);
  static Color whiteColor = Colors.white;

  static List<Color> mainColors = [
    mainColor,
    secondColor,
    thirdColor,
  ];

  static getRandomColor() {
    Random random = Random();
    int index = random.nextInt(mainColors.length);
    return mainColors[index];
  }

  static LinearGradient mainGradientColor = const LinearGradient(
    colors: [
      Color(0xFF397796),
      Color(0xFF14506b),
      Color(0xFF083c53),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static LinearGradient reversedMainGradientColor = const LinearGradient(
    colors: [
      Color(0xFF083c53),
      Color(0xFF14506b),
      Color(0xFF397796),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  static LinearGradient lightGradientColor = const LinearGradient(
    colors: [
      Color(0xFF14506b),
      Color(0xFF14506b),
      Color(0xFF397796),
      Color(0xFFFFFFFF),
    ],
    stops: [0.0, 0.2, 0.6, 0.9],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
  static MaterialColor primaryColor = MaterialColor(0xFF083c53, _color);
  static final Map<int, Color> _color = {
    50: const Color.fromRGBO(20, 80, 107, .1),
    100: const Color.fromRGBO(20, 80, 107, .2),
    200: const Color.fromRGBO(20, 80, 107, .3),
    300: const Color.fromRGBO(20, 80, 107, .4),
    400: const Color.fromRGBO(20, 80, 107, .5),
    500: const Color.fromRGBO(20, 80, 107, .6),
    600: const Color.fromRGBO(20, 80, 107, .7),
    700: const Color.fromRGBO(20, 80, 107, .8),
    800: const Color.fromRGBO(20, 80, 107, .9),
    900: const Color.fromRGBO(20, 80, 107, 1),
  };
}
