import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:salesperson/Model/sales_person_model.dart';

class AdminController extends GetxController {
  final TextEditingController employeeNumberController =
      TextEditingController();
  final TextEditingController fullNameController = TextEditingController();
  final List<String> regions = [
    'Lebanon Region',
    'Coastal Region',
    'Southern Region',
    'Northern Region',
    'Eastern Region',
  ];
  final RxString selectedRegion = ''.obs;
  XFile? image; // Added property

  TextEditingController nameController = TextEditingController();
  TextEditingController regionController = TextEditingController();
  TextEditingController uniqueNumberController = TextEditingController();
  final salespersons = <Salesperson>[
    Salesperson(
        id: 386, name: "Hussam", region: "Lebanon", photo: "assets/hussam.jpg"),
          Salesperson(id: 1, name: 'John Doe', region: 'Region A'),
    Salesperson(id: 2, name: 'Jane Smith', region: 'Region B'),
    Salesperson(id: 3, name: 'Mike Johnson', region: 'Region C'),
  ].obs;

  void addSalesperson(Salesperson salesperson) {
    salespersons.add(salesperson);
  }

  void deleteSalesperson(int salespersonId) {
    salespersons.removeWhere((element) => element.id == salespersonId);
  }
}
