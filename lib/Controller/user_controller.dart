import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';


class UserController extends GetxController {
  TextEditingController nameController = TextEditingController();
  TextEditingController regionController = TextEditingController();
  TextEditingController uniqueNumberController = TextEditingController();
//############################ LogIn
  TextEditingController logInUserNameController = TextEditingController();
  TextEditingController logInPasswordNameController = TextEditingController();
//############################ LogIn
  TextEditingController signUpUserNameController = TextEditingController();
  TextEditingController signUpPasswordNameController = TextEditingController();
  TextEditingController signUpConfirmPasswordNameController =
      TextEditingController();
}
