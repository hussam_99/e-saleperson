import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  TextEditingController salespersonNumberCTRL = TextEditingController();
  TextEditingController salespersonNameCTRL = TextEditingController();
  TextEditingController monthCTRL = TextEditingController();
  TextEditingController registrationDateCTRL = TextEditingController();
  TextEditingController southernregionCTRL = TextEditingController();
  TextEditingController coastalregionCTRL = TextEditingController();
  TextEditingController northernRegionCTRL = TextEditingController();
  TextEditingController easternRegionCTRL = TextEditingController();
  TextEditingController lebanonCTRL = TextEditingController();
  TextEditingController monthlyCommissionCTRL = TextEditingController();
}
