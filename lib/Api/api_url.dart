class API {
  static String login =
      "https://haya244099.pythonanywhere.com/core/auth/jwt/token/";
  static String addSalePerson =
      "https://haya244099.pythonanywhere.com/auth/users/";
  static String modify =
      "https://haya244099.pythonanywhere.com/core/modify/2/"; //PATCH
  static String delete = "https://haya244099.pythonanywhere.com/auth/delete/";
  static String showUser =
      "https://haya244099.pythonanywhere.com/core/user/detail/4/";
  static String enterSellingAmount = "http://127.0.0.1:8000/core/api/sales/";
}
