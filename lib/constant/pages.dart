// ignore_for_file: prefer_const_constructors, depend_on_referenced_packages

import 'package:get/get.dart';
import 'package:salesperson/View/RegularPersonViews/commission_page.dart';
import 'package:salesperson/View/helper/second.dart';
import 'package:salesperson/View/pages/homePage.dart';
import 'package:salesperson/View/pages/log_in.dart';
import 'package:salesperson/View/pages/sign_up.dart';
import 'package:salesperson/View/pages/splash_screen.dart';
import 'package:salesperson/View/helper/third.dart';
import 'package:salesperson/constant/app_route.dart';
import 'package:salesperson/constant/binding.dart';
import 'package:salesperson/constant/transitions.dart';

List<GetPage> listOfPages = [
  GetPage(
    name: AppRoute.splash,
    page: () => const SplashScreen(),
  ),
  GetPage(
    name: AppRoute.login,
    page: () => LoginPage(),
  ),
  GetPage(
    name: '/second',
    page: () => Second(),
    customTransition: SizeTransitions(),
    binding: SampleBind(),
  ),
  // GetPage with default transitions
  GetPage(
    name: '/third',
    transition: Transition.cupertino,
    page: () => const Third(),
  ),
  GetPage(
    name: '/sigup',
    customTransition: SizeTransitions(),
    page: () => SignUpView(),
  ),
  GetPage(
    name: '/calc',
    customTransition: SizeTransitions(),
    page: () => CommissionPage(),
  ),
  GetPage(
    name: '/home',
    customTransition: SizeTransitions(),
    page: () => HomePage(),
  ),
/*  GetPage(
    name: '/calc',
    transition: Transition.cupertino,
    page: () => CommissionPage(),
  ),
  GetPage(
    name: '/calc',
    transition: Transition.cupertino,
    page: () => CommissionPage(),
  ),
  GetPage(
    name: '/calc',
    transition: Transition.cupertino,
    page: () => CommissionPage(),
  ),
  GetPage(
    name: '/calc',
    transition: Transition.cupertino,
    page: () => CommissionPage(),
  ),
*/
];
