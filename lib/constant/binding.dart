// ignore_for_file: depend_on_referenced_packages

import 'package:get/get.dart';
import 'package:salesperson/Controller/admin_controller.dart';
import 'package:salesperson/Controller/controller.dart';
import 'package:salesperson/Controller/home_controller.dart';
import 'package:salesperson/Controller/user_controller.dart';

class SampleBind extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserController>(() => UserController());
    Get.lazyPut<AdminController>(() => AdminController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<ControllerX>(() => ControllerX());
  }
}
